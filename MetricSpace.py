import numpy as np

class MetricSpace:
    def __init__(self, Element, Distance):
        self.Element = Element
        self.Distance = Distance
    
class RnEuclideanSpace(MetricSpace):
    def __init__(self, n):

        def Element(entries):
            if len(entries) != n:
                raise IndexError
            return np.array(entries)
        
        def Distance(a, b):
            return np.linalg.norm(a-b)
        
        self.n = n

        super().__init__(Element, Distance)