import numpy as np
import random as rand

def generate_gaussian_samples(num_gaussians, dim, centers, widths, num_samples):
    samples = np.empty((0, dim))
    for _ in range(num_samples):
        idx = rand.randint(0, num_gaussians-1)
        mean = centers[idx]
        cov = widths[idx] * np.identity(dim)
        sample = np.random.multivariate_normal(mean, cov)
        samples = np.append(samples, [sample], axis=0)
    return samples