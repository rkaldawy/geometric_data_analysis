from sklearn.neighbors import NearestNeighbors
import numpy as np
import math

from KMeansAlgorithm import k_means_clustering, k_means_graph
from MetricSpace import RnEuclideanSpace

def weight(a, b, sigma = 1, space=RnEuclideanSpace(2)):
  dist = space.Distance(a, b)
  return math.exp((-1 * (dist ** 2)) / (2 * (sigma ** 2)))

def spectral_clustering(X, k):
  n = X.shape[0]
  nbrs = NearestNeighbors(n_neighbors=4, algorithm='ball_tree').fit(X)
  _, neighbors = nbrs.kneighbors(X)
  
  # Create the adjacency matrix.
  A = np.zeros((n, n))
  for i in range(neighbors.shape[0]):
    row = neighbors[i]
    for neighbor in row:
      A[i][neighbor] = 1
      A[neighbor][i] = 1
  np.fill_diagonal(A, 0)

  # Apply weight to the adjacency matrix (for now, 1)
  W = np.zeros((n, n))
  for i in range(n):
    for j in range(n):
      W[i, j] = weight(X[i,:], X[j,:])
  W = np.multiply(W, A)

  # Create the degree matrix.
  D = np.zeros((n, n))
  for i, elt in np.ndenumerate(np.sum(W, axis=0)):
    D[i][i] = elt

  # Create the Laplacian matrix.
  L = D - W

  # Calculate eigenvectors and eigenvalues.
  evals, evecs = np.linalg.eig(L)
  evecs = (evecs.T[evals.argsort()]).T
  evals = np.sort(evals)

  # Run k-means clustering.
  samples = evecs[:, 0:k]
  clusters, _ = k_means_clustering(samples, k, 0.01, RnEuclideanSpace(k))

  return clusters