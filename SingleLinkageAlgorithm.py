import numpy as np
import matplotlib.pyplot as plt
import random as rand
from scipy.sparse.csgraph import connected_components

from MetricSpace import RnEuclideanSpace

def single_linkage_clustering(points, epsilon, space=RnEuclideanSpace(3)):
    points = np.array(points)
    adj_matrix = np.empty((0, points.shape[0]))
    for point in points:
        d = np.apply_along_axis(space.Distance, 1, points, point)
        adj = np.less_equal(d, epsilon).astype('int')
        adj_matrix = np.append(adj_matrix, [adj], axis=0)
    np.fill_diagonal(adj_matrix, 0)

    components = connected_components(adj_matrix)
    buckets = [np.empty((0, space.n)) for i in range(components[0])]
    for idx, component_id in enumerate(components[1]):
        buckets[component_id] = np.append(buckets[component_id], [points[idx]], axis=0)
    
    return adj_matrix, buckets

def single_linkage_graph(samples, adj_matrix, buckets):
    plt.clf()
    ax = plt.axes()
    colors = np.array([[1, 0.3, 0.3], [0, 0.5, 1], [0.6, 1, 0.4]])

    for i in range(adj_matrix.shape[0]):
        for j in range(i, adj_matrix.shape[1]):
            if adj_matrix[i][j] == 1:
                x = [samples[i][0], samples[j][0]]
                y = [samples[i][1], samples[j][1]]
                ax.plot(x, y, c='k', zorder=0)

    for idx in range(len(buckets)):
        bucket = buckets[idx]
        if idx < 3:
            color = np.array([colors[idx, :]])
        else:
            color = np.array([np.random.rand(3)])
        ax.scatter(bucket[:, 0], bucket[:, 1], c=color, zorder=10)

    return ax

def single_linkage_graph_3d(samples, adj_matrix, buckets):
    plt.clf()
    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    colors = np.array([[1, 0.3, 0.3], [0, 0.5, 1], [0.6, 1, 0.4]])

    for idx in range(len(buckets)):
        bucket = buckets[idx]
        if idx < 3:
            color = np.array([colors[idx, :]])
        else:
            color = np.array([np.random.rand(3)])
        ax.scatter(bucket[:, 0], bucket[:, 1], bucket[:, 2], c=color, zorder=10)

    return ax