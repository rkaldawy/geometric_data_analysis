import numpy as np
import matplotlib.pyplot as plt
import random as rand
from scipy.sparse.csgraph import connected_components

from MetricSpace import RnEuclideanSpace

def k_clustering(points, k, epsilon, space=RnEuclideanSpace(3), median=False): 
    num_points = points.shape[0]
    point_max, point_min = points.max(axis=0), points.min(axis=0)

    #centroids = np.multiply(np.random.rand(k, space.n), (point_max - point_min)) + point_min
    centroids = np.empty((k, space.n))
    for i in range(k):
        centroids[i] = points[np.random.randint(points.shape[0]), :]
    print(centroids)

    while True:
        clusters = np.zeros((num_points, 1))
        for i in range(num_points):
            point = points[i]
            dist = np.apply_along_axis(space.Distance, 1, centroids, point)
            cluster_id = np.where(dist == np.min(dist))[0][0]
            clusters[i] = cluster_id
        
        centroids_old = np.copy(centroids)
        c_delta = np.zeros((k, 1))
        
        for id in range(k):
            cluster = points[np.where(clusters == id)[0], :]
            if median:
                centroids[id] = np.median(cluster, axis=0)
            elif cluster.size != 0:
                centroids[id] = (1 / cluster.shape[0]) * np.sum(cluster, 0) 
            c_delta[id] = space.Distance(centroids[id], centroids_old[id])


        if np.max(c_delta) < epsilon:
            return clusters, centroids

def k_medians_clustering(points, clusters, epsilon, space=RnEuclideanSpace(3)): 
    return k_clustering(points, clusters, epsilon, space, True)

def k_means_clustering(points, clusters, epsilon, space=RnEuclideanSpace(3)): 
    return k_clustering(points, clusters, epsilon, space, False)

def k_means_graph(bucket_history, centroid_history):
    plt.clf()
    ax = plt.axes()
    buckets, centroids = bucket_history[-1], centroid_history[-1]
    colors = np.array([[1, 0.3, 0.3], [0, 0.5, 1], [0.6, 1, 0.4]])
    colors = np.append(colors, np.random.rand(centroids.shape[0], 3), axis=0)
    for idx in range(len(buckets)):
        bucket = buckets[idx]
        centroid = centroids[idx]
        color = np.array([colors[idx, :]])
        ax.scatter(bucket[:, 0], bucket[:, 1], c=color)
        ax.scatter(centroid[0], centroid[1], c=color, marker="^",  edgecolor='black', linewidth=1, s=60)

    return ax

def k_means_graph_3d(bucket_history, centroid_history):
    plt.clf()
    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    buckets, centroids = bucket_history[-1], centroid_history[-1]
    colors = np.array([[1, 0.3, 0.3], [0, 0.5, 1], [0.6, 1, 0.4]])
    colors = np.append(colors, np.random.rand(centroids.shape[0], 3), axis=0)
    for idx in range(len(buckets)):
        bucket = buckets[idx]
        centroid = centroids[idx]
        color = np.array([colors[idx, :]])
        ax.scatter(bucket[:, 0], bucket[:, 1], bucket[:, 2], c=color)
        ax.scatter(centroid[0], centroid[1], centroid[2], c=color, marker="^",  edgecolor='black', linewidth=1)

    return ax

"""
def k_clustering(points, k, epsilon, space=RnEuclideanSpace(3), median=False): 
    num_points = points.shape[0]
    point_max, point_min = points.max(axis=0), points.min(axis=0)
    centroids = np.multiply(np.random.rand(k, space.n), (point_max - point_min)) + point_min

    while True:
        clusters = np.zeros((num_points, 1))
        for i in range(num_points):
            point = points[i,:]
            dist = np.apply_along_axis(space.Distance, 1, centroids, point)
            cluster_id = np.where(dist == np.min(dist))[0][0]
            clusters[i] = cluster_id
        
        centroids_old = np.copy(centroids)
        c_delta = np.zeros((k, 1))
        for id in range(k):
            cluster = points[np.where(clusters == id)[0], :]
            if median:
                centroids[id] = np.median(cluster, axis=0)
            elif cluster.size != 0:
                centroids[id] = (1 / cluster.shape[0]) * np.sum(cluster, 0) 
            c_delta[id] = space.Distance(centroids[id], centroids_old[id])


        if np.max(c_delta) < epsilon:
            return clusters, centroids

def k_medians_clustering(points, clusters, epsilon, space=RnEuclideanSpace(3)): 
    return k_clustering(points, clusters, epsilon, space, True)

def k_means_clustering(points, clusters, epsilon, space=RnEuclideanSpace(3)): 
    return k_clustering(points, clusters, epsilon, space, False)
"""